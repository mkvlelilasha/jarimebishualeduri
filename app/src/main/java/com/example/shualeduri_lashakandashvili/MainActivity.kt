package com.example.shualeduri_lashakandashvili

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    lateinit var plate1: EditText
    lateinit var plate2: EditText
    lateinit var plate3: EditText
    lateinit var piradin: EditText
    lateinit var shemocmeba: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        plate1 = findViewById(R.id.plate1)
        plate2 = findViewById(R.id.plate2)
        plate3 = findViewById(R.id.plate3)
        piradin = findViewById(R.id.piradin)
        shemocmeba = findViewById(R.id.shemocmeba)

        shemocmeba.setOnClickListener {
            val p1 = plate1.text.toString()
            val p2 = plate2.text.toString()
            val p3 = plate3.text.toString()
            val pn = piradin.text.toString()

            val asotua = ".*[a-z].*".toRegex()

            if((p1.length == 2 && p1.matches(asotua)) && p2.length == 3 && (p3.length == 2 && p1.matches(asotua)) && pn.length == 11) {
                Toast.makeText(this,"შენგაიხარე!", Toast.LENGTH_SHORT).show()
            }
            else {
                Toast.makeText(this,"შეიყვანეთ მონაცემები სწორად!", Toast.LENGTH_SHORT).show()
            }

        }

    }
}